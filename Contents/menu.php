<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
     <div class="collapse navbar-collapse" >
		  <ul class="nav navbar-nav">
			<li <?php 
			$active = 'class="active"';
			if($_SERVER['REQUEST_URI']=="/index.php") 
				echo $active;
			?>><a href="index.php">Kezdőlap</a></li>
			<li <?php 
			if($_SERVER['REQUEST_URI']== "belepes.php") 
				echo $active;
			?>><a href="belepes.php">Belépés</a></li>
			<li <?php
			if($_SERVER['REQUEST_URI'] == "ujSzemet.php")
				echo $active;
			?>><a href="ujSzemet.php">Új elszállítás</a></li>

			  <li <?php
			  if($_SERVER['REQUEST_URI'] == "osszesSzemet.php")
				  echo $active;
			  ?>><a href="osszesSzemet.php">Összes szemét elszállításának megtekintése </a></li>

			<li><a class="btn btn-danger" href="#">Kilépés</a></li>
		  </ul>
	 </div>
  </div>
</nav>