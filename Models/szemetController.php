<?php
session_start();

// KÉRÉS TIPUS vizsgálat
if($_SERVER["REQUEST_METHOD"] == "POST" ){
    if(isset($_POST["ujPalya"])){
        $palyaNev = $_POST["palyaName"];
        $palyaCim = $_POST["palyaCim"];
        $palyaFhely= $_POST["ferohely"];


        require_once("Models/Palya.php");
        //Model statikus insert függvény hívása
        $result= Palyaclass::insertNewMusic($palyaNev,$palyaCim,$palyaFhely);
        if(!$result)
            $errorMsg= "Sikertelen mentés";
        else
            $successMsg = "Sikeres mentés";

    }else if (isset($_POST["modosit"])){
        require_once("Models/Palya.php");
        $palyaNev=$_POST["name"];
        $palyaId=$_POST["palya_id"];
        $palyaFhely =$_POST["ferohely"];
        $result= Palyaclass::updateMusicName($palyaNev,$palyaId,$palyaFhely);

        if(!$result)
            $errorMsg="Sikertelen mentés";
        else
            $successMsg="Sikeres mentés";
    }
//összes eddigi mentett lekérdezés
    //	$html = Palyaclass::getAllWithView();

}else if($_SERVER["REQUEST_METHOD"] == "GET"){
    require_once("Models/Palya.php");
    $html = Palyaclass::getAllWithView();

    /*$adatok = Music::getAll();

    foreach ($adatok as $adat) {
        $html = "Nev: ".$adat["nev"]." Leirás: ".$adat["leiras"]."<br>";*/

}
