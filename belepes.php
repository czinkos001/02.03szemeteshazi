<?php require_once("Controllers/userController.php");?>
<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
<?php require_once("Contents/headIncludes.php")?>
</head>
<body>
	<!-- -->
	<?php if(isset($_SESSION["user"])):
		require_once("Contents/menu.php");	
	?><div class="jumbotron">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 text-center"> 
						<h1>Üdv itthon</h1>
					</div>
				</div>
			</div>
		</div>
	<?php else:?>
	
	<div class="jumbotron">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 text-center"> 
					<h1>Belépés</h1>
				</div>
			</div>
		</div>
	</div>
	<?php if(isset($errorMsg)):?>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 alert alert-danger">
				<?= $errorMsg?>
				</div>
			</div>
		</div>
	<?php endif;?>
	<form class="container form-horizontal" method="POST" action="">
		<div class="form-group">
			<label class="col-sm-4 control-label" for="email">Email cím</label>
			<div class="col-sm-4">
				<input class="form-control" name="email" id="email">
			</div>
		</div>
		<div class="form-group">
			<label for="password" class="col-sm-4 control-label"> Jelszó</label>
			<div class="col-sm-4">
				<input class="form-control" name="password" type="password" id="password">
			</div>
		</div>
		<div class="col-sm-12 col-md-4 col-md-offset-4 ">
			<button type="submit" class="btn btn-block btn-primary">
			Belépés
			</button>		
		</div>
	</form>
	<?php endif; ?>
</body>
</html>

<?php require_once("Contents/headIncludes.php");
require_once("Contents/menu.php") ?>
