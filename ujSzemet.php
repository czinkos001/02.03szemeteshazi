<?php require_once("Controllers/szemetController.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <title>Uj Pálya lista</title>
    <?php require_once("Contents/headIncludes.php") ?>
</head>
<body>

<?php if(isset($_SESSION["user"])):
    require_once("Contents/menu.php");
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <!-- Uj zene lista -->
                <div class="jumbotron">
                    <h2>Új Pálya lista felvétele</h2>
                </div>
                <?php if(isset($errorMsg)):?>
                    <div class="row">
                        <div class="col-sm-12 alert alert-danger text-center">
                            <?=$errorMsg?>

                        </div>
                    </div>

                <?php elseif(isset($successMsg)):?>
                    <div class="row">
                        <div class="col-sm-12 alert alert-success text-center">
                            <?=$successMsg?>

                        </div>
                    </div>
                <?php endif; ?>
                <form method="POST" action="" >
                    <div class="form-group row">
                        <label class="col-sm-4 control-label" for="address">Cím</label>
                        <div class="col-sm-4">
                            <input class="form-control" name="address" id="address">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 control-label" for="date">Szállítás napja</label>
                        <div class="col-sm-4">
                            <input class="form-control" name="date" id="date">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 control-label" for="weight">Becsült súly</label>
                        <div class="col-sm-4">
                            <input class="form-control" name="weight" id="weight">
                        </div>

                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 control-label" for="selective">Szelektív-e?</label>
                        <div class="col-sm-4">
                            <input type="checkbox" class="form-control" name="selective" id="selective" >

                        </div>

                    </div>
                    <div class="row text-right">
                        <input type="submit" class="btn btn-primary" value="Mentés" name="ujSzemet">
                    </div>
                </form>
            </div>

        </div>
    </div>
<?php else: ?>
    <div class="alert alert-danger">
        <h1>Illetéktelen betolakodó</h1>
    </div>
    <div class="alert alert-warning">
        <a href="index.php"><button class="btn btn-primary">Belépés</button><a>
    </div>
<?php endif;?>

</body>
</html>

}
